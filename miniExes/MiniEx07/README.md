[RunMe](https://cdn.staticaly.com/gl/Kragh/ap2019/raw/master/miniExes/MiniEx7/index.html)


Note: 
Emily and I created the code together. We found it very difficult to imagine what the 3 rules could be so we chose to seek inspiration in other already existing codes. 
The source code our program is based upon is found [here] (https://p5js.org/examples/simulate-brownian-motion.html). We later formulated and wrote our rules into the program. 

What are the rules in your generative program and describe how your program performs over time?

- the line is turning to the left / and is randomly exploring the canvas. 
- Lines that connect dots 
- the white line is restricted to the space of the sqaure. 

Our program is almost like an artpiece of lines - everytime you start it, it will take a different path than before. So the pattern of the lines is unique.
Over time the lines will create some kind of web. The web will over time be more and more complex. While the blue line will cover more and more of the page, the white line will will stay in the square. Both lines 
will be mixed together over time and become harder and harder to tell apart. 

What's the role of rules and processes in your work?


What's generativity and automatism? How does this mini-exericse help you to understand what might be generativity and automatism? (see the above - objective 3 and the assigned readings)

I have understood generativity and automatism as a definition for when a program can peform by it self - without inputs from the human-programmer. So by creating rules instead of simple commands in code the computer can 
interpretate/execute them with bigger difference and randomness. I am not sure if this specific miniEx made me think more about generativity and automatism because I think it is a general 
fascination in this process of learning to code - how to make rules for the computer to follow and from these rules it creates something complex and (often unpredictable) beautiful. 

![Screenshot](SB7.JPG)