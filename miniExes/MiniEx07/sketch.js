let num = 200;
let range = 50;

let box1 = 200;
let box2 = 500;
let canvas1 = 800;
let canvas2 = 800;

let boxX = canvas1/2-box1/2;
let boxY = canvas2/2-box2/2;

let ax = [];
let ay = [];

let bx = [];
let by = [];



function setup() {
  createCanvas(canvas1, canvas2);
  background(0);

  //starting point
  for ( let i = 0; i < num; i++ ) {
    ax[i] = boxX+box1/2;
    ay[i] = boxY+box2/2;
    bx[i] = 1;
    by[i] = 1;
  }
  frameRate(10);

  fill(0);
  stroke(255);
    rect(boxX, boxY, box1, box2);
}

function draw() {
  // background(0, 5);

  // Shift all elements 1 place to the left
  for ( let i =0; i < num; i++ ) {
    ax[i - 1] = ax[i];
    ay[i - 1] = ay[i];
  }

  for ( let i = 0; i < num; i++ ) {
    bx[i - 1] = bx[i];
    by[i - 1] = by[i];
  }
  // Put a new value at the end of the array
  ax[num - 1] += random(-range, range);
  ay[num - 1] += random(-range, range);

  bx[num - 1] += random(-range, range);
  by[num - 1] += random(-range, range);

  // Constrain all points to the screen
  ax[num - 1] = constrain(ax[num - 1], boxX, boxX+box1);
  ay[num - 1] = constrain(ay[num - 1],  boxY, boxY+box2);

  bx[num - 1] = constrain(bx[num - 1], 30, 800);
  by[num - 1] = constrain(by[num - 1],  0, 800);

  // Draw a line connecting the points

  for ( let j = 1; j < num; j++ ) {
    let val = j / num*204.0;
    stroke(val);
    strokeWeight(1);
    stroke(255, 5);

    line(ax[j - 1], ay[j - 1], ax[j], ay[j]);

      stroke(255, 255, 255);
      strokeWeight(floor(random(5)));
    line(bx[j - 0.3], by[j - 1], bx[j], by[j]);

  push();
  stroke(20, 0, 200, 40);
  strokeWeight(1);
  line(bx[j - 1], by[j - 1], bx[j], by[j]);


  pop();




  }
}
