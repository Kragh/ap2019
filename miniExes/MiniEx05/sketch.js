let t = 0; // time variable
let y = 300;

//loading jesus image
function preload() {
  year0 = loadImage('giphy.gif')
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  noStroke()
  background(0);

}

function draw() {


  //background with low alpha makes trails on the throbbers
background(0,10);

//glitch effekt and jesus image
if (mouseIsPressed) {
  image(year0, random(400,700), 100);
  fill(random(255),random(255), random(255));
  textSize(frameCount*0.1);
  text('DO YOU KNOW WHAT TIME (IT) IS?', 0, 50);
}
  // make a line of ellipses
  for (let x = 0; x <= width; x = x + 60) {


      // emakes a throbber
      const myX = x + 20 * cos(2 * PI * t + 1);
      const myY = y + 20 * sin(2 * PI * t + 1);

      fill(255, 70);
      ellipse(myX, myY, 15); // draw throbber

  }

  t = t + 0.009; // update time

}
