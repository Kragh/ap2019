[RUNME MiniEx_5](https://glcdn.githack.com/Kragh/ap2019/raw/master/miniExes/MiniEx05/index.html)


	○ what is the concept of your work?
The concept of this piece is time and how we see and understand time. I have further more tried to explore how time is visualized and perceived. I will try and explain the thought chain behind the different
elements in the program. 

line of throbbers: 
I wanted to do a line of the traditionel throbber I made in the miniEx_3 as a study of how we preceive time in different cultures. The circle throbber that goes on and on in an ongoing 
spiral or cirkulation (depends how you perceive it) is a symbol of a circular time, a time that repeat itself and where past, present and future is connected. 
When the throbbers are visually presented in a line they become part of a linear representation of time - or some bigger shape the viewer can't see the end of. Now maybe the throbbers are maybe memories, days, hours or people - all being part of 
a bigger time-picture. 

Press for Jesus/year0: 
The glitching picture of Jesus on the cross I wanted to implement a hidden clue to the intention behind the throbber line - and make the element of time even more cultural connected. The fact that the way we see time in a lot of western cultures is somewhat 
based upon religion - and that our understanding of life and death - and there fore the way we live might be connected to the fact that we perceive time very linear. The birth of Jesus somehow became the start of our time, even though we still recorgnize that time/life existed before. That for me is 
a interesting element and I wanted to bring that into the program. 

text: Do you know what time (it) is?:
With this hidden question (appears when mouse is pressed along side the Jesus image) I wanted to make the audience think about their own perception of time and how it affect our daily lifes. We relate to time everyday and often ask the question: do you know what time it is? but rarely the question do you know what time is? 
Because what is it - a controlling element in our life? a guidance? a wheel? a line? a beginning or an end? Or maybe everything at the same time. 

	○ what is the departure point?


	○ what do you want to express?
what have you changed and why?

![Screenshot](SB2.JPG)