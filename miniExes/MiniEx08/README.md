**MiniEx8 : Generate an electronic literature : Poetry for the moment to come**

[RunMe](https://glcdn.githack.com/Kragh/ap2019/raw/master/miniExes/MiniEx08/index.html)


![Screenshot](SB8.JPG)



The program is made in collaboration with Cecilie Frandsen [GitLab profile](https://gitlab.com/ckf97/my-collection-ap2019). 


We started out by gathering and sharing inspiration so we collectively got a better idea of what we were aiming for aesthetically.
Next, we decided on what concept we wanted to explore with our program. The subject we chose was "The moment of anticipation" inspired by the typing indicator found in a lot of chatrooms, indicating that the person you are chatting with, is about to respond/is typing. 
When this icon appears you feel the time stretch and the moment feels longer in the wait for an answer or a message. 
We found this moment interesting to explore by generating an electronic poem - we imagined the poem to be unpredictable, so the anticipation of what to come was present in the program. 


**3 poems become 1**

We wanted to create our poem using a JSON file. We, therefore, went searching for already existing poems that in some way could portray the moment of anticipation. 
We ended up choosing these 3 poems and the lines we included in the JSON file are highlighted with yellow: 

![Screenshot](poems.JPG)


**vocable code and language**

In the source code, we tried to 'play' with the name of the variables in an attempt to add one more layer to our code/poem. But in the end, the poetry of the executed code and the source code does not have much to do with each other.  The structure in the source code, on the other hand, approved a lot due to our enhanced focus on the presentation of the source code. 
I find the idea of source code adding an extra layer to a program interesting and the idea of code being poems - and not just a tool, is both fascinating and makes the reading and writing code like an adventure where the executed program is a surface you can dig into and explore the words behind. 

