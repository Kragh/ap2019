
let bottles = [];
let bottleimg;
let ocean;
//number of bottles
var num = 8;

function preload() {
  bottleimg = loadImage('bottle2.png');
  ocean = loadImage('ocean.jpg')
}

function setup() {
  createCanvas(windowWidth, windowHeight);

  for (let i = 0; i < num; i++) {
    bottles[i] = new Bottle(10, floor(random(windowHeight)), random(1,4), 100);
  }
}






function draw() {
  //background image
image(ocean, 0, 0, windowWidth, windowHeight);

  //class. see page myClass.js
for (let i = 0; i < num; i++) {
  bottles[i].show();
  bottles[i].move();
  }


}


//function that should remove the bottles when you click on them.
//This part is not working as it should, but I have no idea how to do it another way
//I have tried different solutions but all of them seem to crash the program
function mousePressed() {
  for (let i = bottles.length - 1; i >= 0; i--) {
    if (bottles[i].contains(mouseX, mouseY)) {
      bottles.splice(i,1);
    }
  }
}
