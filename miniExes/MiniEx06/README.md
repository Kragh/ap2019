[RUNME](https://cdn.staticaly.com/gl/Kragh/ap2019/raw/master/miniExes/MiniEx6/index.html)


**The game I had in mind - Plastic ocean game**

I was thinking to make a game where you would collect something - and when you reached a certain number you won.  My inspiration for this kind of game came from the chapter *Collision Callbacks* at this website [reference](https://creative-coding.decontextualize.com/making-games-with-p5-play/). I then saw that the code was primarely build around Sprites and decided to try and 
write my own version using classes and object oriented programming. I then saw that I had 2 or 3 objects were required to do the game - 1) the object that are being collected, 2) the cursor/mouse/player object and 3) the score. I decided to give the game an enviremental message instead of a capitalistic/ opportunistic one - by making the player collect trash/plastic bottles instead of money. 

1) Collectable object 
The properties of this object would be some kind of visible shape or figure. The object was going to occur a lot of different places in the program - so a class for this object would be beneficial. 

*The objects to collect in the plastic ocean game is plastic bottles. The objects properties is made from a png-image and the behavior is going from one side of the canvas to the other is if the bottles were floating in the stream*
*Another behavoir of this object was for it to go away when clicked on or rolledover - but I didn't figure out to do that, which is very frustrating when Daniel Shiftman makes it look so easy in his video. If you have any idea what could be wrong with the syntaxes please feel free to comment!* 

2) The cursor/ the player
This object would contribute to the players understanding and visualisation of interactions with the program. In this case the objects would behave according to the position of the mouse (mouseX & mouseY)
The object would only occur one place at a time. 

*I didn't get to this part of my game - but the cursor I had in mind was to do a hand maybe created as a animation, that would grab when the player clicked the mouse and removed a bottle*
*For this part of the code I would use the p5 play library and the mouseispressed function and probably the variables mouseX & mouseY.*

3) The score 
I am not quite sure whether the score would be an object. But for it to be visualised to the player how many collectable obejcts they collected - some kind of counting object would be necessary. The score should count down or up until the number that triggers the 
victory of the game is reached. 

*This part of the program would have been placed in the upper right corner of the canvas. 



![Screenshot](SB6.JPG)