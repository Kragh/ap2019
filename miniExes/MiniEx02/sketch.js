let capture;
var x = 0;


function setup() {
  createCanvas(600, 600);
  capture = createCapture(VIDEO);
  //capture.hide();

}

function draw() {

//webcam/cirkel
  background(0);
  image(capture, -700, -100, 2000, 1000);
  push();
    stroke(255);
    strokeWeight(400);
    noFill();
    ellipse(300,300,800,800);
  pop();

  push();
  //øjne
    fill(255, x);
    noStroke();
    circle(210, 250, 25);
    circle(390, 250, 25);
  pop();

  push();
  //mund
    noFill();
    stroke(255, x)
    strokeWeight(25)
    arc(300, 350, 200, 150, 0, PI);
  pop();

  textSize(20);
  fill(0,0,0);
  text('Press mouse for happy face', 10, 30);
  textSize(10)
  text('Press      to save             happy face', 10, 50);
  fill(random(255), random(255), random(255));
  text('          S              YOUR', 10, 50);


  if (mouseIsPressed) {
      x = 250;
    } else {
      x = 0;
    }
}

function keyTyped(){
  if (key === "s"){
    saveCanvas("MyEmojiMyFace");
  }
}
