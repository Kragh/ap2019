[RUN ME Mini_Ex2](https://glcdn.githack.com/Kragh/ap2019/raw/master/miniExes/MiniEx2/index.html)

*Describe your program and what you have used and learnt*

When I look and reflect upon my program it is the “if functions” that comes to my mind first. I wanted the traditional smiley-face to appear only when the audience interacted with the code by pressing down the mouse, in that way the smiley could be used as a template for the perfect happy face – but by disappearing give the user/audience an opportunity to make their own emoji and reflect on how their face and their smile is actually looking in reality. In this code I also used push() and pop() for the first time and found that very useful when building the different shapes. Because I didn’t figure out how to shape the video input I also used the push and pop to make a stroke on an ellipse that would cover the rest of the canvas. 


*What have you learnt from reading the assigned reading*? 

I have learned that the digital world very much reflects the real world and the topics that are affecting the society. I learned that emojies as well as programing and code is a language – and by its increasing complexity the world is demanding even more characters for it to vividly describe the world. 


*How would you put your emoji into a wider cultural context that concerns identification, identity, race, social, economics, culture, device politics and beyond? *

As I described in the first chapter Your_Happy_Face gives the audience/user an opportunity to reflect upon what is a happy face. By seeing your own face compared to the traditional smiley the smiley suddenly seem very unemotional and unhuman. And if you try to copy its smile you probably don’t look very happy. Another thought I have given this piece is how society is demanding people to be happy and their best self all the time – and that demand often have the opposite effect because people are using their happy faces when they are actually sad. 

![ScreenShot](ME2.png)