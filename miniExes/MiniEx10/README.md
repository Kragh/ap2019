**Mini_Ex_10 - Flow chart**



***Individuel***

I chose to do a flow chart based on MiniEx08 because I think that the program contains a lot of actions and the code has a lot of variations.
Technically the program has a lot of actions - and there are several objects in the program. 

The challenges of doing a flow chart for this code was to figure out how to present it in an understandable way without making it to technical.
Besides that I found it challenging to figure in what order to present the different parts of the code.  

[Hyperlink to miniEx08 (gitlab)](https://gitlab.com/Kragh/ap2019/tree/master/miniExes/MiniEx08)

[Runme miniEx_08](https://glcdn.githack.com/Kragh/ap2019/raw/master/miniExes/MiniEx08/index.html)

[Source code miniEx_08](https://gitlab.com/Kragh/ap2019/blob/master/miniExes/MiniEx08/sketch.js)

![Screenshot](miniex10.jpg)

***Group***

*Flow chart 1*

![Screenshot](fc1.png)

What might be the possible technical challenges for the two ideas and how are you going to solve them?

The possible technical challenges of this program are creating a JSON-file or database that is vibrant enough for the program to be unpredictable. 
On the other hand this program has more conceptual challenges because it is presenting people and could easily be prejudiced. The technical challenge is in a way how to collect the data 
and could also be seen more as a conceptual one. 


*Flow chart 2* 

![Screenshot](fc2.png)

What might be the possible technical challenges for the two ideas and how are you going to solve them?

The technical challenge of this program is to connect specific data to moveable obejcts. Also the challenge here how to change the whole visual apperance of the program/the data connected 
by one click. Here a way of solving this could be object oriented programming - where the interactable-dots in the web is build into a class that can be assigned specific data. 


*Individual*: How is this flow chart different from the one that you had in #3 (in terms of the role of a flow chart)?
I am not sure what flow chart #3 is - and there fore find i a bit difficult to answer the question. But I guess that these two flow chart is less specific technically because the coding is not done yet - and there are 
several ways to write a program. 

*Individual*: If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would you reflect upon the notion of algorithms? (see if you could refer to the text and articulate your thoughts?)


