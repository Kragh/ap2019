[RUNME_4](https://glcdn.githack.com/Kragh/ap2019/raw/master/miniExes/MiniEx04/index.html)

**MiniEx_4 CAPTURE ALL - Hello stranger, take a look at my memories**

*Hello stranger, take a look at my memories* is created with the intention to comment on the effect of the share culture and facebook and how it changed our view of what is private and what can be shared. 
I have had a facebook profile for over 10 years - years where I have  been tagged in pictures and uploaded pictures myself. Facebook has over the years become a mermory lane for both me, my friends and strangers. 
The classic photoalbum has been replaced and made public, and even more for the younger generations. The pictures of pregnancy, first baby pictures, birthdays and so on is being shared with the world. 
But what is the issue in this? 
The issue might be the social effect the like economy has birthed.  The following quote shines a light on missing negative side to the digital social life. Here only the bright sides are shown or maybe a well dressed negative view from time to time. 
this understanding of faceboook as a memory lane becomes one sided, and the life you are presenting is primarely social depended or selected by yourself. 

> Facebook has always thought that anything that is social in the world should be social online’ (Gelles, 2010). In contrast, we would like to outline that there are limits to Facebook’s enclosure of sociality, most notably in the current absence of the widely requested Dislike button as a critical counterpart to the Like button. (1, page 15(8))

The use of social media to collect our thoughts, memories and pictures - shortly our life becomes very undivergent and when we look back at our lives through Facebooks picture gallery we only see a
very limited perspective.

(1) Gerlitz, Carolin, and Helmond, Anne. “The like Economy: Social Buttons and the Data-Intensive Web.” New Media & Society 15, no. 8 (December 1, 2013): 1348–65. (can access via e-library)



![Screenshot](SB4.JPG)