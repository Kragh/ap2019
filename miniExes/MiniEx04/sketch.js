

var slider;
var postext = 450;
var tekst = ['2019', '2018', '2017', '2016', '2015', '2014', '2013', '2012', '2011', '2010', '2009', '2008'];

function preload() {
img2019 = loadImage('img1.jpg');
img2018 = loadImage('img2018.jpg');
img2017 = loadImage('img2017.jpg');
img2016 = loadImage('img2016.jpg');
img2015 = loadImage('img2015.jpg');
img2014 = loadImage('img2014.jpg');
img2013 = loadImage('img2013.jpg');
img2012 = loadImage('img2012.jpg');
img2011 = loadImage('img2011.jpg');
img2010 = loadImage('img2010.jpg');
img2009 = loadImage('img2009.jpg');
img2008 = loadImage('img2008.jpg');

}

function setup() {
  createCanvas(1000,1000);
  slider = createSlider(0, 11, 0);
  slider.position(100, 100);
  slider.style('width', '800px');

}

function draw() {


  fill(0);
  textSize(40);

var val = slider.value();

if (val === 0) {
  clear();
  image(img2019, 100,200, 800, 800);
  text(tekst[0], postext, 80)

} else if (val === 1) {
  clear();
  image(img2018, 100,200, 800, 800);
  text(tekst[1], postext, 80)

} else if (val === 2) {
  clear();
  image(img2017, 100,200, 800, 800);
  text(tekst[2], postext, 80)

} else if (val === 3) {
  clear();
  image(img2016, 100,200, 800, 800);
  text(tekst[3], postext, 80)

} else if (val === 4) {
  clear();
  image(img2015, 100,200, 800, 800);
  text(tekst[4], postext, 80)

} else if (val === 5) {
  clear();
  image(img2014, 100,200, 800, 800);
  text(tekst[5], postext, 80)

} else if (val === 6) {
  clear();
  image(img2013, 100,200, 800, 800);
  text(tekst[6], postext, 80);

} else if (val === 7) {
  clear();
  image(img2012, 100,200, 800, 800);
  text(tekst[7], postext, 80);

} else if (val === 8) {
  clear();
  image(img2011, 100,200, 800, 800);
  text(tekst[8], postext, 80);

} else if (val === 9) {
  clear();
  image(img2010, 100,200, 800, 800);
  text(tekst[9], postext, 80);

} else if (val === 10) {
  clear();
  image(img2009, 100,200, 800, 800);
  text(tekst[10], postext, 80)

} else if (val === 11) {
  clear();
  image(img2008, 100,200, 800, 800);
  text(tekst[11], postext, 80)
}


}

// return c-d;
// });
