[RUNME MiniEx_3](https://cdn.staticaly.com/gl/Kragh/ap2019/raw/master/miniExes/MiniEx3/index.html)

**MiniEx_3 Circles_and_Time**

For this program I used Winnie Soon’s program as a base/inspiration for my re-design (see link to source-code below) I chose to make it more minimalistic. I then added another throbber because I thought that it would illustrate how small changes in the appearance of a shape can trick the eye. Kind of like an optic illusion. The inner circle seems to be slower than the outer circle because the ellipses are bigger and closer together but actually both circles are spinning with the exact same speed and consist of the same amount of ellipses. 
I used the frameCount to make the color of the throbbers change over time – so that you actually see some kind of progress (of course until it reaches the limit 255) in the program. 

[link to the source-code by Winnie Soon](https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/class03/sketch03/sketch03.js)

![ScreenShot](SBthrobber.JPG)