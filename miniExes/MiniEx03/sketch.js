

function setup() {
 createCanvas(windowWidth, windowHeight);
 background(255);
 frameRate (20);
}

function draw() {

  fill(255, 10);
  noStroke();
  rect(0, 0, width, height);
  drawThrobber(100,100);
}

function drawThrobber(num) {
  push();
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(0,0, frameCount*0.2);
  ellipse(40, 10, 40, 40);
  translate(width/16, height/16);
  ellipse(50, 20, 8, 8);
  pop();






}
