[miniEx1](https://cdn.staticaly.com/gl/Kragh/ap2019/raw/master/miniExes/MiniEx1/index.html)

[miniEx1B](https://cdn.staticaly.com/gl/Kragh/ap2019/raw/master/miniExes/MiniEx1B/index.html)

**Circle of sqaures**

When I started exploring p5 I made the goal to figure out how to make a transparent color on a 3d object. 
I also explored how moving the background from function setup to functoin draw changed the outcome. I used random in both the color and the rotation syntax 
because I liked how the figure became less stiff and predictable. 

I explored the syntaxes 
*  random 
*  scale 
*  3d objects 
*  fill
*  stroke 
*  opacity 
*  framerate

I also in other projects explored the possibility of using images in p5. But had trouble combining them with the 3d objects. 
I find that when I am coding the best results comes from the unexpected, because I did not imagined the possible outcome. 

![Screenshot](SSminiex1.JPG)
![Screenshot](SSminiex1B.JPG)